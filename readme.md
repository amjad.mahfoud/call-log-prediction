Dataset2 Documentation

Number of Instances: 500

Number of Attributes: 6

Attribute Information:
-- Complete attribute documentation: 
1 Type: phone call type 1 for incomming, 2 for outgoing, 3 for missed linear
2 Day of week:linear
3 Month: linear
4 Day of month: linear
5 Hour: The hour of call, linear
6 ContactId: , linear

Missing Attribute Values: None.

Number of classes: 15

Class Distribution:

Contact ID : Number of instances: Notes
-1: 47 #Unknown caller id
63: 1
102: 4
112: 54
124: 18
591: 33
613: 1
707: 40
720: 71
721: 2
723: 43
726: 1
730: 149
757: 35
831: 1